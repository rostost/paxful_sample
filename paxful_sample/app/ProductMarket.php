<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMarket extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'product_market';
}
