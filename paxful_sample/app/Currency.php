<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{



  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'currency';

    public function currency_rates()
    {
       return $this->hasMany(CurrencyRate::class);
    }


    /**
    * @return CurrencyRate
    */
    public function recent_rate(){
      //return $this->hasOne(CurrencyRate::class)->latest()->id;

      return $this->currency_rates()->orderBy('actual_date', 'DESC')->first();
    }

}
