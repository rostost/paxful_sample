<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyRate extends Model
{


  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'currency_rate';

  /**
  * Get the currency record accosiated with
  */
  public function currency()
  {
    // return $this->hasOne('App\Currency');

    return $this->belongsTo(Currency::class);
  }











}
