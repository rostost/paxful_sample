<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWallet extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'user_wallet';
}
