<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'offer';


  CONST STATUS_AVAILABLE = 100;
  CONST STATUS_IN_PROGRESS = 200;
  CONST STATUS_EXPIRED = 300;
  CONST STATUS_COMPLETED = 400;
  CONST STATUS_CANCELLED = 500;


  public function user(){
    return $this->belongsTo(User::class)->first();
  }


  public function interest_percent(){
    $percent = $this->interest * 100;
    return round($percent, 5);
  }

  public function product_item(){
    return $this->belongsTo(ProductItem::class)->first();
  }


  public function product_code(){
    return $this->product_item()->product()->first()->machine_name;
  }

  public function current_rate_price(){
    return $this->product_item()->id;
  }

}
