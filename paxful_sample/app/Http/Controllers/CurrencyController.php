<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      //$Currency = new \App\Currency();
      $data = \App\Currency::orderBy('iso', 'ASC')->get();

      //Dummy records
      // foreach($data as $item){
      //   if($item->recent_rate() == NULL){
      //     $CurrencyRate = new \App\CurrencyRate();
      //     $CurrencyRate->currency_id = $item->id;
      //     $CurrencyRate->rate_value = 1.0;
      //     $CurrencyRate->actual_date = '2019-05-01 00:01';
      //     $CurrencyRate->save();
      //   }
      // }



        //print_r($data);

        return view('pages/currency_list', ['data' => $data]);
    }
}
