<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OfferController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($user_id = false)
    {
      if(!$user_id){
        $offers = \App\Offer::get();
      }else{
        $user = \App\User::findOrFail($user_id);
        if(is_null($user)){
          //@TODO message that user problem
          return redirect('/');
        }else{
          $offers = \App\Offer::where('user_id', $user->id)->get();
        }
      }

        return view('pages/offer_list', ['data' => $offers]);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new()
    {
        return view('pages/offer_new');
    }





    /**
   * Create a new flight instance.
   *
   * @param  Request  $request
   * @return Response
   */
    public function store(Request $request)
    {

        if($request->input('interest') > 300){
            //@TODO error out based on percent amount
        }

        $Offer = new \App\Offer();

        //We store interest in floats where 1 = 100%
        $Offer->interest = $request->input('interest') / 100;
        $Offer->user_id = Auth::user()->id;
        $Offer->status = $Offer::STATUS_AVAILABLE;


        $ProductItem = Auth::user()->product_items()->first();

        if(is_null($ProductItem)){
            //@TODO error out based on BTC availability for user
        }
        $Offer->product_item_id = $ProductItem->id;

        $amount = (float)$request->input('amount');

        if($ProductItem->value < $amount){
            //@TODO error out based on offer amount
        }

        //@TODO validation for min Product amount (check Product Min Amout field)

        //Get valut from User BTC amount and move it into Offer
        $ProductItem->value -= $amount;
        $Offer->amount = $amount;

        //Store Objects
        $Offer->save();
        $ProductItem->save();

        return redirect('offer/list');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function my_offers()
    {
        //Just get current user and render user offers page
        return $this->user_offers(Auth::user()->id);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function user_offers($user_id)
    {
      $user = \App\User::findOrFail($user_id);
      if(is_null($user)){
        return redirect('/');
      }

      $data = \App\Offer::where('user_id', $user->id)->get();


      //Validate if you have permissions
      return view('pages/offer_user_offerlist', [
          'loaded_user' => $user,
          'data'  => $data,
        ]);
    }









}
