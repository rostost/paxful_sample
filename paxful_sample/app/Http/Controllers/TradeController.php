<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TradeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new($offer_id)
    {

      $Offer = \App\Offer::findOrFail((int)$offer_id);

      if(is_null($Offer)){
        //@TODO error out as offer not found
        return redirect('offer/list');
      }

      $PaymentMethods = \App\PaymentMethod::get();

        return view('pages/trade_new', [
          'offer' => $Offer,
          'payment_methods' => $PaymentMethods,
        ]);
    }


    /**
   * Create a new flight instance.
   *
   * @param  Request  $request
   * @return Response
   */
    public function store(Request $request)
    {
        $Trade = new \App\Trade();

        $Offer = \App\Offer::findOrFail((int)$request->input('offer_id'));
        if(is_null($Offer)){
          //@TODO error out as offer not found
          return redirect('offer/list');
        }

        if($Offer->status !== $Offer::STATUS_AVAILABLE){
          //@TODO error out as offer not available for trade
          return redirect('offer/list');
        }


        $Trade->offer_id = $Offer->id;
        $Trade->status = $Trade::STATUS_PENDING;
        $Trade->user_id = Auth::user()->id;
        $Trade->product_market_id = 1; //HArdcorded for now = BTC current value
        $Trade->currency_id = 147; //Hardcoded to USD for now

        $Trade->payment_method_id = (int)$request->input('payment_method');
        $Trade->offer_id = $Offer->id;

        //Add calculated Trade money to Seller Wallet
        /*

          $money =
            $Offer->amount    //BTC amount
              *
            $Offer->Product_item->currency->rate     //BTC market value on Trade creation date
              *
            $OFfer->interest_rate    //Seller interest_rate
              *
            $Trade -> payment_method -> comission      //Payment Method comission  (commission must be >= min_commission)
            'amount_to_seller' => $amount_to_seller,
            'amount_to_charge_buyer' => $amount_to_charge_buyer
          ];
        */
        $Trade->final_value = $this->calculateTradePrice($Trade,$Offer)['amount_to_seller'];

        $Offer->status = $Offer::STATUS_IN_PROGRESS;

        //Store Objects
        $Trade->save();
        $Offer->save();

        return redirect('trade/summary/' . $Trade->id);
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function summary($trade_id)
    {

      $Trade = \App\Trade::findOrFail((int)$trade_id);


      //Validate if you have permissions
      return view('pages/trade_summary', [
          'trade' => $Trade
        ]);
    }

    /**
   * Create a new flight instance.
   *
   * @param  Request  $request
   * @return Response
   */
    public function complete(Request $request)
    {
        $trade_id = (int)$request->input('trade_id');
        //@TODO be sure trade by ID from form submit is owned by current user

      //Close Trade
      $Trade = \App\Trade::findOrFail((int)$trade_id);
      $Trade->status = $Trade::STATUS_SUCCESS;

      //Close and Offer
      $Offer = $Trade->offer();
      $Offer->status = $Offer::STATUS_COMPLETED;

      //Adding BTC to user who was a Trade Owner
      $ProductItem = $Offer->product_item();
      if(is_null($ProductItem)){
        //Add Product Item to new registered user_offers
        $productItem = new \App\ProductItem();
        $productItem->user_id = $Trade->user()->id;
        $productItem->product_id = 1; //BTC
        $productItem->value = 0; // BTC amaount
      }
      $ProductItem->value += $Offer->amount; // BTC amaount
      $ProductItem->save();
      $Offer->save();
      $Trade->save();



      //Validate if you have permissions
      return redirect('offer/list');
    }



    public function calculateTradePrice($Trade, $Offer){

      $seller_interest = $Offer->interest;
      $btc_market = 3000; //Hardcode for now, can be loaded from PRoductMarket table
      $offer_amount = $Offer->amount;
      $payment_method_comission = $Trade->payment_method()->relative_comission;

// print('|||');
//       print_r($seller_interest);
//         print('|||');
//       print_r($btc_market);
//       print('|||');
//       print_r($offer_amount);
//       print('|||');
//       print_r($payment_method_comission);

      $amount = $btc_market*$offer_amount;

      //Append Seller %;
      $amount_to_seller = $amount + $amount*$seller_interest;

      //Append Payment comission
      $amount_to_charge_buyer = $amount_to_seller + ($amount_to_seller / 100 * $payment_method_comission); //It was incorrectly stored in DB as integer of 100%

      //print("Expected price for $offer_amount BTC = $price USD");

      //exit();

      return [
          'amount_to_seller' => $amount_to_seller,
          'amount_to_charge_buyer' => $amount_to_charge_buyer
        ];
    }








}
