<?php

/**
*   Trade is a short-time living item which need to have expiration date and fixed money amount in selected currency
*
*/

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'trade';

  CONST STATUS_PENDING = 200;
  CONST STATUS_SUCCESS = 300;
  CONST STATUS_CANCELLED = 400;


  /**
  * Get the currency record accosiated with
  */
  public function offer()
  {
    // return $this->hasOne('App\Currency');

    return $this->belongsTo(Offer::class)->first();
  }

  /**
  * Get the currency record accosiated with
  */
  public function user()
  {
    // return $this->hasOne('App\Currency');

    return $this->belongsTo(User::class);
  }


  /**
  * Get the currency record accosiated with
  */
  public function currency()
  {
    // return $this->hasOne('App\Currency');

    return $this->belongsTo(Currency::class);
  }


  /**
  * Get the currency record accosiated with
  */
  public function payment_method()
  {
    // return $this->hasOne('App\Currency');

    return $this->belongsTo(PaymentMethod::class)->first();
  }






}
