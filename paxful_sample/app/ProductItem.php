<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductItem extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'product_item';


  /**
  * Get the currency record accosiated with
  */
  public function product()
  {
    // return $this->hasOne('App\Currency');

    return $this->belongsTo(Product::class);
  }



}
