<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Turn On login Routes
Auth::routes();

Route::get('/', function () {
    return view('welcome_paxful');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/offer/list', 'OfferController@index')->name('offer_list');
Route::get('/offer/list/{user_id}', 'OfferController@index')->name('offer_list_user');
Route::get('/offer/new', 'OfferController@new')->name('offer_new');
Route::post('/offer/store', 'OfferController@store')->name('offer_store');
Route::get('/offer/my-offers', 'OfferController@my_offers')->name('offer_my_offers');


Route::get('/trade/new/{offer_id}', 'TradeController@new')->name('trade_new');
Route::post('/trade/store', 'TradeController@store')->name('trade_store');
Route::get('/trade/summary/{trade_id}', 'TradeController@summary')->name('trade_summary');
Route::post('/trade/complete', 'TradeController@complete')->name('trade_complete');

Route::get('/currency/list', 'CurrencyController@index')->name('currency_list');
