<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_method', function (Blueprint $table) {
            $table->increments('id', true);
            $table->timestamps();

            $table->string('machine_name')->unique();
            $table->string('name');
            $table->float('relative_comission', 11, 10);
            $table->float('min_comission_value', 11, 10);

            $table->integer('min_comission_currency_id')->unsigned()->index();
            $table->foreign('min_comission_currency_id')->references('id')->on('currency');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_method');
    }
}
