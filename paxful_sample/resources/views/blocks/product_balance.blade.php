<div class="row">
  <div class="col col-md-4">My Product Balance</div>
  <div class="col col-md-8">
    @foreach ( Auth::user()->product_items()->get() as $product_item )
      <h3>{{ $product_item->value }} {{ $product_item->product->machine_name }}</h3>
    @endforeach
  </div>
</div>
