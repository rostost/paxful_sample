@extends('layouts.app')

@section('page_title')
  Welcome to {{ config('app.name', 'Laravel') }}
@endsection

@section('content')
<div class="flex-center position-ref full-height">
    <!-- @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif -->

    <div class="content">
        <div class="title m-b-md">
            Lets start Trading?!
        </div>

        <div class="links">
            <a href="{{ route('offer_list') }}">Offers</a>
            <!-- <a href="{{ route('home') }}">Payment Methods </a>
            <a href="{{ route('home') }}">Trades</a> -->
            <a href="{{ route('currency_list') }}">Currencies and Rates</a>


        </div>
    </div>
</div>
@endsection
