@extends('layouts.app')

@section('page_title')
  My Dashboard
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h4>Dashboard</h4></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                      <div class="col col-md-4">User</div>
                      <div class="col col-md-8"><h4>{{ Auth::user()->name }}</h3></div>
                    </div>
                    <div class="row">
                      <div class="col col-md-4">Email</div>
                      <div class="col col-md-8"><h4>{{ Auth::user()->email }}</h3></div>
                    </div>
                    <div class="row">
                      <div class="col col-md-4">My Wallet Balance</div>
                      <div class="col col-md-8"><h3>0,00 USD</h3></div>
                    </div>

                    @include('blocks/product_balance')
                </div>
            </div>
            <br/>
            <div class="card">
              <div class="card-header"><h4>Favoutites</h4></div>
              <div class="card-body">
                <a href="{{ url('/') }}">Home</a>
                <a href="{{ route('offer_list') }}">All Offers</a>
                <a href="{{ route('offer_list_user', Auth::user()->id) }}">My Offers</a>
                <a class="" href="{{ route('offer_new') }}">Create New Offer</a>
            </div>

    </div>
    </div>
</div>
@endsection
