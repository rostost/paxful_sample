@extends('layouts.app')

@section('page_title') Create New Offer @endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">@
      @include('blocks/product_balance')
    </div>
    <br/>

    <div class="row justify-content-center">
        <form method="POST" action="{{ route('offer_store') }}">
          <div class="row">
                @csrf
                <div class="form-group row">
                    <label for="amount" class="col-md-4 col-form-label text-md-right">{{ __('I want to sell') }}</label>

                    <div class="col-md-8">
                        <input id="amount" type="text" class="form-control" name="amount" value="0" autofocus>
                        BTC
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group row">
                    <label for="interest" class="col-md-4 col-form-label text-md-right">{{ __('Interest') }}</label>

                    <div class="col-md-8">
                        <input id="interest" type="text" class="form-control" name="interest" value="0">%
                    </div>
                </div>
          </div>
          <div class="row">
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-6">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Make Offer') }}
                        </button>
                    </div>
                </div>
            </div>
       </form>


   </div>
</div>
@endsection
