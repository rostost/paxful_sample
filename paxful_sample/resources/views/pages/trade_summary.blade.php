@extends('layouts.app')

@section('page_title') Trade for {{ $trade->offer()->user()->name }} Offer @endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">

        <form style="width: 80%;" method="POST" action="{{ route('trade_complete') }}">

          <div class="row">
                @csrf
          </div>

          <div class="trade-summary-wrapper">
            <div class="row">
                <div class="col-md-6 "><h3>Total Trade Price</h3>( without Payment method comission )</div>
                <div class="col-md-6 "><h3>{{ $trade->final_value }} USD</h3></div>
            </div>

        </div>

        <input id="" type="hidden" class="form-control" name="trade_id" value="{{ $trade->id }}" />

        <br/><br/>
          <div class="row">
                <div class="form-group row mb-0">
                    <div class="col-md-6 ">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Complete ') }}
                        </button>

                    </div>
                </div>
            </div>
            <small>Normally Complete should be triggered once Payment method proceeded</small>
       </form>


   </div>
</div>
@endsection
