@extends('layouts.app')

@section('page_title') Trade for {{ $offer->user()->name }} Offer @endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">

        <form style="width: 80%;" method="POST" action="{{ route('trade_store') }}">
          <p class="col-md-12">Hurry Up, somebody may trade for this Offer at the same time. <br/>{{ $offer->user()->name }} will have more money, but you will have more Coins!</p>


          <div class="row">
                @csrf
                <div class="form-group row">
                    <label for="interest" class="col-md-4 col-form-label text-md-right">{{ __('Payment Method') }}</label>

                    <div class="col-md-8">
                        <select id="payment_method_select" name="payment_method">
                          @foreach ($payment_methods as $payment_method)
                            <option data-commission="{{ $payment_method->relative_comission }}" value="{{ $payment_method->id }}">{{ $payment_method->name }} (fee: {{ $payment_method->relative_comission }} %)</option>
                          @endforeach
                        </select>
                    </div>
                </div>
          </div>

          <div class="trade-summary-wrapper">
            <h3>Summary</h3>
            <div class="row">
                <div class="col-md-6">Offer BTC Amount</div>
                <div class="col-md-6">{{ $offer->amount }}</div>
            </div>
            <div class="row">
                <div class="col-md-6">Offer Seller Interest</div>
                <div class="col-md-6 ">{{ $offer->interest_percent() }} %</div>
            </div>
            <div class="row">
                <div class="col-md-6 ">BTC Market Price</div>
                <div class="col-md-6 ">3000 USD</div>
            </div>
            <div class="row">
                <div class="col-md-6 ">Payment Method Comission</div>
                <div class="col-md-6 "><span id="payment_commission_val">JS needed</span> %</div>
            </div>
            <div class="row">
                <div class="col-md-6 ">Total Trade Price</div>
                <div class="col-md-6 "><span id="total_price">JS with Ajax should be here</span></div>
            </div>

        </div>

        <input id="amount" type="hidden" class="form-control" name="offer_id" value="{{ $offer->id }}" />


          <div class="row">
                <div class="form-group row mb-0">
                    <div class="col-md-6 ">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Agree to Trade, Lets finish ') }}
                        </button>
                    </div>
                </div>
            </div>
       </form>


   </div>
</div>
@endsection
