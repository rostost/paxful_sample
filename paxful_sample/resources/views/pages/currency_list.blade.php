@extends('layouts.app')

@section('page_title') Currency List & Actual Rates @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <table class="table table-stripped">
        <thead>
          <tr>
            <th class="col-id">Id</th>
            <th class="col-iso">ISO</th>
            <th class="col-name">Name</th>
            <th class="col-rate">Latest Rate to USD</th>
            <th class="col-rate">Rate Date</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $data_item)
          <tr>
            <td class="col-id">
              {{ $data_item->id }}
            </td>
            <td class="col-iso">
              {{ $data_item->iso }}
            </td>
            <td class="col-name">
              {{ $data_item->name }}
            </td>
            <td class="col-name">
              @if ($data_item->recent_rate() != NULL)
                  <div>{{ $data_item->recent_rate()->rate_value }}</div>
              @else
                  -- no rate received --
              @endif
            </td>
            <td class="col-name">
              @if ($data_item->recent_rate() != NULL)
                  <div>{{ $data_item->recent_rate()->actual_date }}</div>
              @else
                  --
              @endif
            </td>

          </tr>
          @endforeach
        </tbody>

        </table>
    </div>
</div>
@endsection
