@extends('layouts.app')

@section('page_title') Offers List @endsection

@section('content')
<div class="container">

  <div class="row">
    <div>
      <a class="btn btn-primary" href="{{ route('offer_new') }}">New Offer</a>
    </div>
  </div>

    <div class="row justify-content-center">
      <table class="table table-stripped">
      <thead>
        <tr>
          <th class="col-id">Id</th>
          <th class="col-amount">Item</th>
          <th class="col-amount">Market Price</th>
          <th class="col-created">Created On</th>
          <th class="col-interest">Seller Interest</th>
          <th class="col-username">Seller</th>
          <th class="col-action">Actions</th>

        </tr>
      </thead>
      <tbody>
        @foreach ($data as $offer)
        <tr class=@if ( $offer->user()->id == Auth::user()->id ) "offer-own" @endif>
          <td class="col-id">
            {{ $offer->id }}
          </td>
          <td class="col-created">
            {{ $offer->amount }} BTC
          </td>
          <td class="col-created">
            {{ $offer->amount }} USD
          </td>
          <td class="col-created">
            {{ $offer->created_at }}
          </td>
          <td class="col-interest">
            {{ $offer->interest_percent() }} %
          </td>
          <td class="col-username">
            {{ $offer->user()->name }}
          </td>
          <td class="col-action">
            @if ( $offer->status == $offer::STATUS_AVAILABLE )
              @if ( $offer->user()->id == Auth::user()->id )
                <button data-offer-id="" class="btn btn-danger">Cancel</button>
              @else
                <a href="{{ route('trade_new', $offer->id) }}" class="btn btn-primary">Lets Trade</a>
              @endif
            @elseif($offer->status == $offer::STATUS_IN_PROGRESS)
              -- Trade in Progress --
            @else
              -- Complete --
            @endif


          </td>
        </tr>
        @endforeach
      </tbody>

      </table>


    </div>
</div>
@endsection
